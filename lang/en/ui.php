<?php

return [
    // Homepage

    'welcome' => "Welcome to Presto.it",
    'categories' => "Categories",
    "show" => "Show",
    '6announcements' => "Last 6 announcements",
    "details" => "Go to details",
    "search"=>"Search",

    // Dettaglio annunci
    
    'category' => "Category",
    'date' => "Posted on",
    'user' => "Posted by",

    // Navbar

    'ourannouncements' => "Announcements",
    'insertannouncement' => "Post an announcement",
    "login" => "Login",
    'register' => "Sign in",
    "logout" => "Logout",
    'revisor' => "Revisor",

    // I nostri annunci

    'announcements' => 'Announcements',

    // Inserisci il tuo annuncio

    "announce"=>"Announce",
    "successmessage" => "Announcement created successfully! (awaiting review)",
    "postad" => "Post your announcement",
    "articlename" => "Article name",
    "price" => "Price",
    "description" => "Description",
    "choosecategory" => "Choose category",
    "image"=>"Images",

    // Revisore

    "revisorpagetitle" => "Revisors section",
    "accept" => "Accept",
    "refuse" => "Refuse",
    "noadstoreview" => "There is no announcements to review.",

    // Ricerca

    "searchresults" => "Results for:",

    // Registrazione

    "username"=>"Username",
    "email"=>"Email",
    "conemail"=>"Confirm email",
    "password"=>"Password",
    "conpassword"=>"Confirm password",
    "submit"=>"Submit",
];