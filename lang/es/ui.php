<?php

return [
    // Homepage

    'welcome'=>"Bienvenidos en Presto.it",
    'categories' => "Categorías",
    "show" => "Vista",
    '6announcements' => "Los ultimos 6 anuncios",
    "details" => "Ir al detalle",
    "search"=>"Buscar",

    // Dettaglio annunci
    
    'category' => "Categoría",
    'date' => "Publicado en",
    'user' => "Publicado por",

    // Navbar

    'ourannouncements' => "Anuncios",
    'insertannouncement' => "Publica un anuncio",
    "login" => "Iniciar sesión",
    'register' => "Registrarse",
    "logout" => "Cerrar sesión",
    'revisor' => "Revisor",

    // I nostri annunci

    'announcements' => 'Anuncios',

    // Inserisci il tuo annuncio

    "announce"=>"Anuncio",
    "successmessage" => "Anuncio creato con èxito! (pendiente de revisión)",
    "postad" => "Publica tu anuncio",
    "articlename" => "Nombre del articulo",
    "price" => "Precio",
    "description" => "Descripción",
    "choosecategory" => "Elige la categoría",
    "image"=>"Imágenes",

    // Revisore

    "revisorpagetitle" => "Sección de los revisores",
    "accept" => "Acepta",
    "refuse" => "Niega",
    "noadstoreview" => "No hay anuncios para revisar.",

    // Ricerca

    "searchresults" => "Resultados de búsqueda de:",

    // Registrazione

    "username"=>"Nombre de usuario",
    "email"=>"Dirección de correo electrónico",
    "conemail"=>"Confirmar dirección de correo electrónico",
    "password"=>"Contraseña",
    "conpassword"=>"Confirmar contraseña",
    "submit"=>"Enviar",
];