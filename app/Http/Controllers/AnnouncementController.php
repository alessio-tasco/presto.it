<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Jobs\ResizeImage;
use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Models\AnnouncementImage;
use App\Jobs\GoogleVisionLabelImage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Jobs\GoogleVisionSafeSearchImage;
use App\Http\Requests\AnnouncementRequest;
use App\Jobs\GoogleVisionRemoveFaces;

class AnnouncementController extends Controller
{
    public function __construct() {
        $this->middleware("auth")->except("index");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function detail(Announcement $announcement)
    {
        return view("announcement.detail", compact("announcement"));
    }

    public function index()
    {
        $announcements = Announcement::all();
        return view("announcement.index", compact("announcements"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $categories = Category::all();
        $uniqueSecret = $request->old("uniqueSecret", base_convert(sha1(uniqid(mt_rand())), 16, 36));
        return view("announcement.create", compact("categories", "uniqueSecret"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnnouncementRequest $request)
    {
        $a = Announcement::create([
            'title'=>$request->title,
            'price'=>$request->price,
            'body'=>$request->body,
            'category_id'=>$request->category,
        ]);
        $uniqueSecret = $request->input("uniqueSecret");
        
        $images = session()->get("images.{$uniqueSecret}", []);
        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);

        $images = array_diff($images,$removedImages);

        foreach ($images as $image){
            $i = new AnnouncementImage();

            $fileName = basename($image);
            $newFileName = "public/announcements/{$a->id}/{$fileName}";
            Storage::move($image, $newFileName);

            $i ->file = $newFileName;
            $i ->announcement_id = $a->id;
            
            $i ->save();
            
            GoogleVisionSafeSearchImage::withChain([
            new GoogleVisionLabelImage($i->id),
            new GoogleVisionRemoveFaces($i->id),
            new ResizeImage($newFileName,640, 360),
            new ResizeImage($newFileName,300, 150),
            new ResizeImage($newFileName,400, 300)
            ])->dispatch($i->id);
        }

        File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));

        return redirect(route("insert"))->with("announcement.created.success", "ok");
    }

    public function uploadImage(Request $request)
    {
        $uniqueSecret = $request->input("uniqueSecret");
        $fileName = $request->file("file")->store("public/temp/{$uniqueSecret}");
        dispatch(new ResizeImage(
            $fileName,
                120,
                120
        ));
        session()->push("images.{$uniqueSecret}", $fileName);
        return response()->json([
            'id'=>$fileName
        ]);
    }

    public function removeImage(Request $request)
    {
        $uniqueSecret = $request->input("uniqueSecret");
        $fileName = $request->input("id");
        session()->push("removedimages.{$uniqueSecret}", $fileName);
        Storage::delete($fileName);
        return response()->json("ok");
    }

    public function getImages(Request $request)
    {
        $uniqueSecret = $request->input("uniqueSecret");
        $images = session()->get("images.{$uniqueSecret}",[]);
        $removedImages = session()->get("removedimages.{$uniqueSecret}",[]);
        $images = array_diff($images,$removedImages);
        $data = [];
        foreach ($images as $image){
            $data[] = [
                'id'=>$image,
                'src'=>AnnouncementImage::getUrlByFilePath($image, 120, 120)
            ];
        }
        return response()->json($data);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function show(Announcement $announcement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcement $announcement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announcement $announcement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcement $announcement)
    {
        //
    }

}
