<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\AnnouncementController;
use App\Http\Controllers\RevisorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", [PublicController::class, ("welcome")])->name("welcome");
Route::post("/locale/{locale}", [PublicController::class, ("locale")])->name("locale");

// Rotte per gli annunci
Route::get("/announcement/index", [AnnouncementController::class, ("index")])->name("index");
Route::get("/announcement/create", [AnnouncementController::class, ("create")])->name("insert");
Route::post("/announcement/store", [AnnouncementController::class, ("store")])->name("store");
Route::get("/announcement/{announcement}", [AnnouncementController::class, "detail"])->name("detail");
Route::post("/announcement/images/upload", [AnnouncementController::class, "uploadImage"])->name("images.upload");
Route::delete("/announcement/images/remove", [AnnouncementController::class, "removeImage"])->name("images.remove");
Route::get("/announcement/get/images/", [AnnouncementController::class, "getImages"])->name("images");

// Rotte per ricerca
Route::get("/search",[PublicController::class, "search"])->name("search");

// Rotta categoria card
Route::get("/category/{cat}",[CategoryController::class, ("index")])->name("category");

//Rotte per Revisori
Route::get("/revisor/home", [RevisorController::class, ("index")])->name("revisor.home");
Route::post("/revisor/announcement/{id}/accept", [RevisorController::class, ("accept")])->name("revisor.accept");
Route::post("/revisor/announcement/{id}/reject", [RevisorController::class, ("reject")])->name("revisor.reject");


