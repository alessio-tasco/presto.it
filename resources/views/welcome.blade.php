<x-layout>

    @if (session("announcement.created.success"))
        <div class= "alert alert-success">
            Annuncio creato correttamente.
        </div>
    @endif

    @if (session("access.denied.revisor.only"))
        <div class="alert alert-danger">
            Accesso non consentito, solo per revisori!
        </div>
    @endif

{{-- <div class="container mt-5">
    <div class="row h-100 align-items-center justify-content-center"> 
        <div class="col-12 col-md-2">
            <a class="btn btn-success my-3" href="{{route('insert')}}">Inserisci il tuo annuncio</a>
        </div>
    </div>
</div> --}}
<div class="container masthead mt-0 mx-0">
    <div class="row h-100 align-items-center"> 
        <div class="col-12">
            <div class="text-center">
                <img class="mt-5" src="images/bag.png" style="width:300px;"></div>
                
            {{-- <h1 class="text-center text-dark fw-bold" style="font-size:60px;">Presto.it</h1> --}}
            <form action="{{route('search')}}" method="GET" class="mb-5 mt-3 d-flex justify-content-center">
                <input type="text" class="br searchl" name="q" style="width:500px;" placeholder="{{ __('ui.search')}}"/>
                <button class="btn buttons text-white" type="submit"><i class="fa-solid fa-magnifying-glass" ></i></button>
            </form>
        </div>
    </div>
</div>

{{-- <div class="container mt-5">
    <div class="row justify-content-between">
        <h3 class="d-flex justify-content-center mb-4">{{ __('ui.categories')}}</h3>
        @foreach ($categories as $category)
            <div class="col-12 d-flex justify-content-center col-lg-2">
                <div class="card linecard mb-3 text-center" style="max-width: 12rem;">
                    <div class="card-header">{{$category->name}}</div>
                        <div class="card-body text-danger">
                        <a class="btn buttonc card-title" href="{{route('category', ["cat"=>$category->id])}}">{{ __('ui.show')}}</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div> --}}

<div class="container mt-5">
    <div class="row justify-content-between">
        <h2 class="d-flex justify-content-center mb-4 fw-bold">{{ __('ui.categories')}}</h2>
        @foreach ($categories as $category)
            <div class="col-12 d-flex justify-content-center col-lg-2">
                <div class="cardh linecard mb-3 text-center" style="max-width: 12rem;">
                    {{-- <div class="card-header"><i class="fa-solid fa-magnifying-glass" ></i></div> --}}
                    <div class="card-body text-danger">
                    <a class="btn buttonc card-title" href="{{route('category', ["cat"=>$category->id])}}">{{$category->name}}</a>

                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

{{-- <div class="container mt-5">
    <div class="row justify-content-center">
        <h3 class="text-center">{{ __('ui.6announcements')}}</h3>
        @foreach ($announcements as $announcement)
        <div class ="col-4 d-flex justify-content-center">
            <div class="card linecard my-3 mx-3" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">{{$announcement->title}}</h5>
                    <p class="card-text">{{ __('ui.price')}} : {{$announcement->price}} € </p>
                    @foreach ($announcement->images as $image)
                    @if ($loop->first)
                    <div class="carousel-item active">
                          <img width="100%" height="100%" 
                          src="{{$image->getUrl(640, 360)}}" alt="">
                    </div>
                    @endif
                    @endforeach
                    <p class="card-text">{{$announcement->category->name}} </p>
                    <p class="card-text">{{$announcement->created_at->format('d/m/Y')}} </p>
                    <a href="{{route("detail" , compact("announcement") )}}" class="btn buttonc ">{{ __('ui.details')}}</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div> --}}

<div class="container">
    <div class="row justify-content-center">
        <h2 class="text-center mt-5 mb-3 fw-bold">{{ __('ui.6announcements')}}</h2>
        @foreach ($announcements as $announcement)
        <div class="col-12 col-xl-4">
        <div class="text-center">
                <article class="cardh linecard mx-3 my-3">
                    @foreach ($announcement->images as $image)
                    @if ($loop->first)
                          <img width="100%" height="100%" 
                          src="{{$image->getUrl(640, 360)}}" alt="">
                    @endif
                    @endforeach
                    <div class="card__info">
                        <h3 class="card__title">{{$announcement->title}}</h3>
                        <p class="card-text">{{ __('ui.price')}}: {{$announcement->price}} € </p>
                        <p class="card-text">{{$announcement->category->name}} </p>
                        <p class="card-text">{{$announcement->created_at->format('d/m/Y')}} </p>
                        <a href="{{route("detail" , compact("announcement") )}}" class="btn buttond linecard">{{ __('ui.details')}}</a>
                    </div>
                </article>  
            </div>
        </div>
            @endforeach
    </div>
</div>

</x-layout>