<x-layout>
    <section class="content-section id="portfolio">
        <div class="container">
            <div class="content-section-heading text-center">
                <h1 class="mb-3 mt-3">{{ __('ui.searchresults')}} {{$q}}</h1>

            </div>
            <div class="row">
                @foreach ($announcements as $announcement)

                <div class="card linecard mx-3 my-3" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">{{$announcement->title}}</h5>
                    <p class="card-text">{{$announcement->price}} € </p>
                    <img src="https://via.placeholder.com/150" class="card-img-top" alt="...">
                    <p class="card-text">{{$announcement->body}} </p>
                    <p class="card-text">{{$announcement->category->name}} </p>
                    <p class="card-text">{{$announcement->created_at->format('d/m/Y')}} </p>
                    <a href="{{route("detail" , compact("announcement") )}}" class="btn btn-primary">{{ __('ui.details')}}</a>
                </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
</x-layout>