<x-layout>


@if($errors->any()) 
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <div>{{$error}}</div>
                @endforeach
            </ul>
        </div>
@endif

<div class="container-fluid">
  <div class="row justify-content-center">
    <h2 class="mt-5 mb-3 text-center fw-bold">{{ __('ui.login')}}</h2>
    <div class="col-10 col-md-8 col-lg-6">
      <form method="POST" action="{{route('login')}}">
          @csrf
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Email address</label>
          <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" 
          name="email">
        </div>
        <div class="mb-3">
          <label for="exampleInputPassword1" class="form-label">Password</label>
          <input type="password" class="form-control" id="exampleInputPassword1" name="password">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </div>
</div>

</x-layout>