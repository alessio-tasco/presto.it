<nav class="navbar navbar-expand-lg navbar-dark bgc">
  <div class="container-fluid">
    <a class="navbar-brand" href="{{route('welcome')}}"><i class="fa-solid fa-xl ms-3 fa-store"></i></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="{{route('index')}}">{{ __('ui.ourannouncements')}}</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="{{route('insert')}}">{{ __('ui.insertannouncement')}}</a>
      </li>
    </ul>
    <ul class="navbar-nav me-3 mb-2 mb-lg-0">
      @guest
      <li class="nav-item">
        <a class="nav-link" href="{{route('login')}}">{{ __('ui.login')}}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('register')}}">{{ __('ui.register')}}</a>
      </li>
      @else
      @if (Auth::user()->is_revisor)
        <li class="nav-item">
          <a href="{{route('revisor.home')}}" class="nav-link">{{ __('ui.revisor')}}
            <span class="badge rounded-pill bg-danger text-white" style="font-size: 15px";>{{\App\Models\Announcement::ToBeRevisionedCount()}}</span>
          </a>
        </li>    
      @endif
      <li class="nav-item">
        <a class="nav-link" href="{{route('logout')}}" 
        onclick="event.preventDefault();document.getElementById('logout-form').submit()">{{ __('ui.logout')}}</a>
      </li>
      <li>
      <form id="logout-form" action="{{route('logout')}}" method="POST" class="d-none">
            @csrf
          </form>
      </li>
      @endguest
      <!-- Lingue -->

      <li class="nav-item">
        @include('layouts._locale', ['lang'=>'it', 'nation'=>'it'])
      </li>

      <li class="nav-item">
      @include('layouts._locale', ['lang'=>'en', 'nation'=>'gb'])
      </li>

      <li class="nav-item">
      @include('layouts._locale', ['lang'=>'es', 'nation'=>'es'])
      </li>
      
    </ul>
    </div>
  </div>
</nav>