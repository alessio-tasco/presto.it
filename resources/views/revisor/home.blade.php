<x-layout>
    <h2 class="text-center mt-5 mb-3 fw-bold">
        {{ __('ui.revisorpagetitle')}}
    </h2>

    @if ($announcement)
        
        <div class="container mb-3">
            <div class="row justify-content-center">
                <div class="col-10 col-md-8 col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            {{ __('ui.announce')}} # {{$announcement->id}}
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6"><h3>{{ __('ui.username')}}</h3></div>
                                <div class="col-md-6">
                                    # {{$announcement->user->id}},
                                    {{$announcement->user->name}},
                                    {{$announcement->user->email}},
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6"><h3>{{ __('ui.articlename')}}</h3></div>
                                <div class="col-md-6">{{$announcement->title}}</div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6"><h4>{{ __('ui.description')}}</h4></div>
                                <div class="col-md-6">{{$announcement->body}}</div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>{{ __('ui.image')}}</h4>
                                </div>
                                <div class="col-md-6">
                                    @foreach ($announcement->images as $image)
                                    <div class="row mb-2">
                                        <div class="col-md-4">
                                            <img class="mb-3" src="{{$image -> getUrl(300, 150)}}"
                                            class="rounded" alt="">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <b> Adult </b>: {{$image->adult}}<br>
                                                <b> Medical</b>: {{$image->medical}}<br>
                                                <b> Spoof</b>: {{$image->spoof}}<br>
                                                <b> Violence</b>: {{$image->violence}}<br>
                                                <b> Racy</b>: {{$image->racy}}<br>

                                                <b>Labels:</b> <br>
                                                <ul>
                                                    @if($image->labels)

                                                        @foreach($image->labels as $label)
                                                            <li>{{$label}}</li>
                                                        @endforeach
                                                        
                                                    @endif
                                                </ul>

                                               <b>id : </b>{{$image->id}}<br>
                                                {{$image->file}}<br>
                                                {{Storage::url($image->file)}}<br>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center mt-5">
                <div class="col-6 col-md-4 text-center">
                    <form action="{{route('revisor.reject', $announcement->id)}}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-danger">{{ __('ui.refuse')}}</button>
                    </form>
                </div>
                <div class="col-6 col-md-4 text-center">
                    <form action="{{route('revisor.accept', $announcement->id)}}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-success">{{ __('ui.accept')}}</button>
                    </form>
                </div>
            </div>
        </div>
     @else
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-6">
                    <h3>{{ __('ui.noadstoreview')}}</h3>
                </div>
            </div>
        </div>    
    @endif
</x-layout>